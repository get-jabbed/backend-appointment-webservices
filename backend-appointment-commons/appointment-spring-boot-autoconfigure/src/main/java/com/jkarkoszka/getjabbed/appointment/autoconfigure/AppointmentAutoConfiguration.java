package com.jkarkoszka.getjabbed.appointment.autoconfigure;

import com.jkarkoszka.getjabbed.appointment.service.AppointmentMapper;
import com.jkarkoszka.getjabbed.appointment.web.AppointmentRestMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@AutoConfigureAfter(JpaRepositoriesAutoConfiguration.class)
@Import(DbRepositoryRegistrar.class)
class AppointmentAutoConfiguration {

  @Bean
  AppointmentMapper appointmentMapper() {
    return Mappers.getMapper(AppointmentMapper.class);
  }

  @Bean
  AppointmentRestMapper appointmentRestMapper() {
    return Mappers.getMapper(AppointmentRestMapper.class);
  }
}
