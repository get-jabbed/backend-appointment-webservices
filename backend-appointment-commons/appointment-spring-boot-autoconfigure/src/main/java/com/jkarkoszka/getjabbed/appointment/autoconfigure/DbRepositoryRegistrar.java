package com.jkarkoszka.getjabbed.appointment.autoconfigure;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDb;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

class DbRepositoryRegistrar implements ImportBeanDefinitionRegistrar {

  @Override
  public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata,
                                      BeanDefinitionRegistry registry) {
    AutoConfigurationPackages.register(registry, AppointmentDb.class.getPackageName());
  }
}