package com.jkarkoszka.getjabbed.appointment.db;

/**
 * Appointment statuses.
 */
public enum Status {
  AVAILABLE, PRE_BOOKED, BOOKED, DONE
}
