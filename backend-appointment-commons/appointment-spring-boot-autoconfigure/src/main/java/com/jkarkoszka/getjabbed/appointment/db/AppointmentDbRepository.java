package com.jkarkoszka.getjabbed.appointment.db;

import java.time.LocalDateTime;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Class to work with Appointment DB.
 */
public interface AppointmentDbRepository extends ReactiveMongoRepository<AppointmentDb, ObjectId> {

  @Query("{'vaccineCenterId': ?0, 'at': {$gte: ?1, $lte: ?2}, 'status': 'AVAILABLE'}")
  Flux<AppointmentDb> findAvailableAppointment(String vaccineCenterId, LocalDateTime startAt,
                                               LocalDateTime endAt);

  @Query("{'status': 'PRE_BOOKED', 'updatedAt': {$lte: ?0 }}")
  Flux<AppointmentDb> findPreBookedAppointmentUpdatedBefore(LocalDateTime updatedAtBefore);

  @Query("{id: ?0, 'status': 'PRE_BOOKED', 'patientId': ?1}")
  Mono<AppointmentDb> findPreBookedAppointment(ObjectId id, String patientId);

  @Query("{id: ?0, 'status': 'BOOKED', 'vaccineCenterId': ?1}")
  Mono<AppointmentDb> findBookedAppointment(ObjectId id, String vaccineCenterId);

}
