package com.jkarkoszka.getjabbed.appointment.service;

import com.jkarkoszka.getjabbed.appointment.db.Status;
import java.time.LocalDateTime;
import lombok.Value;

/**
 * Appointment model.
 */
@Value
public class Appointment {

  String id;
  LocalDateTime at;
  String vaccineCenterId;
  String patientId;
  Status status;

}
