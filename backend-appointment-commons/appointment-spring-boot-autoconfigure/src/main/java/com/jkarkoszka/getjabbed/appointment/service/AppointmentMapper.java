package com.jkarkoszka.getjabbed.appointment.service;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDb;
import com.jkarkoszka.getjabbed.db.mapper.ObjectIdMapper;
import org.mapstruct.Mapper;

/**
 * Mapper Appointment/AppointmentDb.
 */
@Mapper(uses = ObjectIdMapper.class)
public interface AppointmentMapper {

  Appointment map(AppointmentDb appointmentDb);

}
