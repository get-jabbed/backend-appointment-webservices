package com.jkarkoszka.getjabbed.appointment.web;

import com.jkarkoszka.getjabbed.appointment.service.Appointment;
import org.mapstruct.Mapper;

/**
 * Mapper AppointmentRest/AppointmentDb.
 */
@Mapper
public interface AppointmentRestMapper {

  AppointmentRest map(Appointment appointment);

}
