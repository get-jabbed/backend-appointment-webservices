package com.jkarkoszka.getjabbed.appointment.web;

import com.jkarkoszka.getjabbed.appointment.db.Status;
import java.time.LocalDateTime;
import lombok.Value;

/**
 * Appointment REST model.
 */
@Value
public class AppointmentRest {

  String id;
  LocalDateTime at;
  String vaccineCenterId;
  String patientId;
  Status status;

}
