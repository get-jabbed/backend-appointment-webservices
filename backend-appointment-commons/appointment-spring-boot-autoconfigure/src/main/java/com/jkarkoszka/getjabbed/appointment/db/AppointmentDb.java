package com.jkarkoszka.getjabbed.appointment.db;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Appointment DB model.
 */
@Document(collection = "appointments")
@Data
@Builder(toBuilder = true)
@SuppressFBWarnings
public class AppointmentDb {

  @Id
  private ObjectId id;

  private LocalDateTime at;

  private String vaccineCenterId;

  private String patientId;

  private Status status;

  @Version
  private Long version;

  private LocalDateTime createdAt;

  private LocalDateTime updatedAt;
}
