package com.jkarkoszka.getjabbed.appointment.autoconfigure;

import static org.assertj.core.api.Assertions.assertThat;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDbRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import testproject.TestApplication;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE, classes = {
    TestApplication.class})
public class AppointmentAutoConfigurationTest {

  @Autowired
  AppointmentDbRepository appointmentDbRepository;

  @Test
  void shouldSetUpAppointmentDb() {
    assertThat(appointmentDbRepository).isNotNull();
  }
}
