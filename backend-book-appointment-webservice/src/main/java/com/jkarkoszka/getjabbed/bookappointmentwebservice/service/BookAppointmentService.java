package com.jkarkoszka.getjabbed.bookappointmentwebservice.service;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDbRepository;
import com.jkarkoszka.getjabbed.appointment.service.Appointment;
import com.jkarkoszka.getjabbed.appointment.service.AppointmentMapper;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * Service to book appointment.
 */
@Component
@AllArgsConstructor
public class BookAppointmentService {

  private final AppointmentDbRepository appointmentDbRepository;
  private final BookedAppointmentUpdater bookedAppointmentUpdater;
  private final AppointmentMapper appointmentMapper;

  /**
   * Method to book appointment.
   *
   * @param bookAppointmentRequest book appointment request payload
   * @return appointment
   */
  public Mono<Appointment> book(BookAppointmentRequest bookAppointmentRequest) {
    return Mono.fromSupplier(bookAppointmentRequest::getAppointmentId)
        .map(ObjectId::new)
        .flatMap(appointmentId -> appointmentDbRepository.findPreBookedAppointment(
                appointmentId, bookAppointmentRequest.getPatientId())
            .switchIfEmpty(Mono.error(new PreBookedAppointmentNotFoundException(appointmentId)))
        )
        .map(bookedAppointmentUpdater::update)
        .flatMap(appointmentDbRepository::save)
        .map(appointmentMapper::map);
  }
}
