package com.jkarkoszka.getjabbed.bookappointmentwebservice.service;

import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Pre-booked appointment not found exception.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class PreBookedAppointmentNotFoundException extends RuntimeException {

  public PreBookedAppointmentNotFoundException(ObjectId appointmentId) {
    super(String.format("Pre-booked appointment '%s' not found.", appointmentId.toHexString()));
  }
}
