package com.jkarkoszka.getjabbed.bookappointmentwebservice.service;

import lombok.Builder;
import lombok.Value;

/**
 * BookAppointmentRequest model.
 */
@Value
@Builder
public class BookAppointmentRequest {

  String appointmentId;
  String patientId;

}
