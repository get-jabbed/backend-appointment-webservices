package com.jkarkoszka.getjabbed.bookappointmentwebservice.web;

import com.jkarkoszka.getjabbed.bookappointmentwebservice.service.BookAppointmentRequest;
import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import org.springframework.security.core.userdetails.User;

class BookAppointmentRequestCreator {

  public static BookAppointmentRequest create(
      String appointmentId,
      GetJabbedAuthenticationToken getJabbedAuthenticationToken) {
    return BookAppointmentRequest.builder()
        .appointmentId(appointmentId)
        .patientId(((User) getJabbedAuthenticationToken.getPrincipal()).getUsername())
        .build();
  }
}
