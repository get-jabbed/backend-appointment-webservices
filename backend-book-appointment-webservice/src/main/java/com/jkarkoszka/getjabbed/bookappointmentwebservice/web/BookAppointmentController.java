package com.jkarkoszka.getjabbed.bookappointmentwebservice.web;

import com.jkarkoszka.getjabbed.appointment.web.AppointmentRest;
import com.jkarkoszka.getjabbed.appointment.web.AppointmentRestMapper;
import com.jkarkoszka.getjabbed.bookappointmentwebservice.service.BookAppointmentService;
import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Controller which exposes endpoint to book appointment.
 */
@RestController
@AllArgsConstructor
public class BookAppointmentController {

  private final BookAppointmentService bookAppointmentService;
  private final AppointmentRestMapper appointmentRestMapper;

  @PatchMapping("/appointments/pre-booked/{appointmentId}")
  Mono<AppointmentRest> findAvailableAppointments(
      @PathVariable("appointmentId") String appointmentId,
      GetJabbedAuthenticationToken getJabbedAuthenticationToken) {
    return Mono.fromSupplier(() -> BookAppointmentRequestCreator.create(appointmentId,
            getJabbedAuthenticationToken))
        .flatMap(bookAppointmentService::book)
        .map(appointmentRestMapper::map);
  }
}
