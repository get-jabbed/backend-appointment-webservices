package com.jkarkoszka.getjabbed.bookappointmentwebservice.service;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDb;
import com.jkarkoszka.getjabbed.appointment.db.Status;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.springframework.stereotype.Component;

@Component
class BookedAppointmentUpdater {

  AppointmentDb update(AppointmentDb appointmentDb) {
    return appointmentDb.toBuilder()
        .status(Status.BOOKED)
        .updatedAt(LocalDateTime.now(ZoneOffset.UTC))
        .build();
  }
}
