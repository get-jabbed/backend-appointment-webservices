package com.jkarkoszka.getjabbed.findappointmentwebservice.service;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDbRepository;
import com.jkarkoszka.getjabbed.appointment.service.Appointment;
import com.jkarkoszka.getjabbed.appointment.service.AppointmentMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service to find available appointment.
 */
@Component
@AllArgsConstructor
@Slf4j
public class FindAppointmentService {

  public static final int EXPECTED_NUMBER_OF_RESULTS = 3;
  public static final int MAXIMUM_RANGE_IN_DAYS = 3;
  public static final int CONCURRENCY = 1;
  private final AppointmentDbRepository appointmentDbRepository;
  private final PreBookedAppointmentUpdater preBookedAppointmentUpdater;
  private final AppointmentMapper appointmentMapper;

  /**
   * Method to find available appointment.
   *
   * @param findAppointmentRequest find appointment requst payload
   * @return appointments
   */
  public Flux<Appointment> findAvailableAppointments(
      FindAppointmentRequest findAppointmentRequest) {
    var endAt = findAppointmentRequest.getStartAt()
        .plusDays(MAXIMUM_RANGE_IN_DAYS);
    return appointmentDbRepository.findAvailableAppointment(
            findAppointmentRequest.getVaccineCenterId(), findAppointmentRequest.getStartAt(),
            endAt)
        .flatMap(foundAppointment -> Mono.fromSupplier(
                () -> preBookedAppointmentUpdater.updateToPreBooked(foundAppointment,
                    findAppointmentRequest.getPatientId()))
            .flatMap(appointmentDbRepository::save)
            .doOnError(OptimisticLockingFailureException.class,
                t -> log.warn("Optimistic lock exception thrown, skip to the next appointment."))
            .onErrorResume(OptimisticLockingFailureException.class, t -> Mono.empty()), CONCURRENCY)
        .take(EXPECTED_NUMBER_OF_RESULTS)
        .map(appointmentMapper::map);
  }
}
