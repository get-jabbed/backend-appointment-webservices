package com.jkarkoszka.getjabbed.findappointmentwebservice.web;

import com.jkarkoszka.getjabbed.appointment.web.AppointmentRest;
import com.jkarkoszka.getjabbed.appointment.web.AppointmentRestMapper;
import com.jkarkoszka.getjabbed.findappointmentwebservice.service.FindAppointmentService;
import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Controller to expose endpoint to find available appointment.
 */
@RestController
@AllArgsConstructor
public class FindAvailableAppointmentsController {

  private final FindAppointmentService findAppointmentService;
  private final AppointmentRestMapper appointmentRestMapper;

  /**
   * Endpoint to find available appointment.
   *
   * @param vaccineCenterId              vaccine center id
   * @param startAt                      start at
   * @param getJabbedAuthenticationToken get jabbed authentication token
   * @return appointments
   */
  @GetMapping("/appointments")
  public Flux<AppointmentRest> findAvailableAppointments(
      @RequestParam("vaccineCenterId") String vaccineCenterId,
      @RequestParam("startAt") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
          LocalDateTime startAt,
      GetJabbedAuthenticationToken getJabbedAuthenticationToken) {
    return Mono.fromSupplier(() -> FindAppointmentRequestCreator.create(vaccineCenterId,
            startAt, getJabbedAuthenticationToken))
        .flatMapMany(findAppointmentService::findAvailableAppointments)
        .map(appointmentRestMapper::map);
  }
}
