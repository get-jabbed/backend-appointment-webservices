package com.jkarkoszka.getjabbed.findappointmentwebservice.service;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDb;
import com.jkarkoszka.getjabbed.appointment.db.Status;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.springframework.stereotype.Component;

/**
 * Update Appointment to pre-booked status.
 */
@Component
public class PreBookedAppointmentUpdater {

  AppointmentDb updateToPreBooked(AppointmentDb appointmentDb, String patientId) {
    return appointmentDb.toBuilder()
        .status(Status.PRE_BOOKED)
        .patientId(patientId)
        .updatedAt(LocalDateTime.now(ZoneOffset.UTC))
        .build();
  }
}
