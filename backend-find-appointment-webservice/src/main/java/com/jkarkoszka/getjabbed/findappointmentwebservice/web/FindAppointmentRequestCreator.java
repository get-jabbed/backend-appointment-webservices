package com.jkarkoszka.getjabbed.findappointmentwebservice.web;

import com.jkarkoszka.getjabbed.findappointmentwebservice.service.FindAppointmentRequest;
import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import java.time.LocalDateTime;
import org.springframework.security.core.userdetails.User;

class FindAppointmentRequestCreator {

  static FindAppointmentRequest create(String vaccineCenterId,
                                       LocalDateTime startAt,
                                       GetJabbedAuthenticationToken getJabbedAuthenticationToken) {
    return FindAppointmentRequest.builder()
        .vaccineCenterId(vaccineCenterId)
        .startAt(startAt)
        .patientId(((User) getJabbedAuthenticationToken.getPrincipal()).getUsername())
        .build();
  }
}
