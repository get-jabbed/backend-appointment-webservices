package com.jkarkoszka.getjabbed.findappointmentwebservice.service.scheduler;

import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Scheduler to clean pre booked appointments.
 */
@Component
@EnableScheduling
@AllArgsConstructor
public class CleanPreBookAppointmentScheduler {

  private final CleanPreBookAppointmentSchedulerTask schedulerTask;

  @Scheduled(cron = "${getjabbed.find-appointment-webservice."
      + "clean-pre-booked-appointment-task-cron-expression}")
  public void executeTask() {
    schedulerTask.executeTask().block();
  }
}
