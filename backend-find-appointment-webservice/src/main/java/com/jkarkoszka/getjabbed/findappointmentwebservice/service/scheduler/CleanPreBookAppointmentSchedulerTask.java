package com.jkarkoszka.getjabbed.findappointmentwebservice.service.scheduler;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDbRepository;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
@Slf4j
class CleanPreBookAppointmentSchedulerTask {

  private final AppointmentDbRepository appointmentDbRepository;
  private final AvailableAppointmentUpdater availableAppointmentUpdater;

  Mono<Void> executeTask() {
    return appointmentDbRepository.findPreBookedAppointmentUpdatedBefore(
            LocalDateTime.now(ZoneOffset.UTC).minusMinutes(1))
        .map(availableAppointmentUpdater::updateToAvailable)
        .flatMap(appointmentDbRepository::save)
        .doOnNext(appointmentDb -> log.info(
            String.format("Appointment %s released from PRE_BOOKED status.",
                appointmentDb.getId().toHexString())))
        .then();
  }
}
