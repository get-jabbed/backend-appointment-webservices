package com.jkarkoszka.getjabbed.findappointmentwebservice.service.scheduler;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDb;
import com.jkarkoszka.getjabbed.appointment.db.Status;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.springframework.stereotype.Component;

/**
 * Class to update appointment to AVAILABLE status.
 */
@Component
public class AvailableAppointmentUpdater {

  AppointmentDb updateToAvailable(AppointmentDb appointmentDb) {
    return appointmentDb.toBuilder()
        .status(Status.AVAILABLE)
        .patientId(null)
        .updatedAt(LocalDateTime.now(ZoneOffset.UTC))
        .build();
  }
}
