package com.jkarkoszka.getjabbed.findappointmentwebservice.service;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Value;

/**
 * Find appointment request payload.
 */
@Value
@Builder
public class FindAppointmentRequest {

  String vaccineCenterId;
  LocalDateTime startAt;
  String patientId;
}
