package com.jkarkoszka.getjabbed.appointmentwebservice.processappointment.service;

import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Booked appointment not found exception.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class BookedAppointmentNotFoundException extends RuntimeException {

  public BookedAppointmentNotFoundException(ObjectId appointmentId, String vaccineCenterId) {
    super(String.format("Booked appointment '%s' in vaccine center '%s' not found.",
        appointmentId.toHexString(), vaccineCenterId));
  }
}
