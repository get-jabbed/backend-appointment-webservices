package com.jkarkoszka.getjabbed.appointmentwebservice.createappointments.service;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Value;

/**
 * Create appoinment request payload.
 */
@Value
@Builder
public class CreateAppointmentsRequest {

  String id;
  LocalDateTime startAt;
  LocalDateTime endAt;
  int howOftenInMinutes;
  int howManyParallel;
  String vaccineCenterId;

}
