package com.jkarkoszka.getjabbed.appointmentwebservice.createappointments.service;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDbRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service to create appointment.
 */
@Component
@AllArgsConstructor
@Slf4j
public class CreateAppointmentService {

  public static final String NEW_APPOINTMENTS_EVENT_TOPIC = "new_appointments2";

  private final KafkaTemplate<String, Object> kafkaTemplate;
  private final AppointmentDbCreator appointmentDbCreator;
  private final AppointmentDbRepository appointmentDbRepository;

  /**
   * Method to create new apppointments.
   *
   * @param createAppointmentsRequest create appointment request
   * @return empty
   */
  public Mono<Void> createNewAppointmentEventsAndSendToQueue(
      CreateAppointmentsRequest createAppointmentsRequest) {
    return createNewAppointmentEvents(createAppointmentsRequest).doOnNext(
        newAppointmentEvent -> kafkaTemplate.send(NEW_APPOINTMENTS_EVENT_TOPIC,
            newAppointmentEvent)).then();
  }

  private Flux<NewAppointmentEvent> createNewAppointmentEvents(
      CreateAppointmentsRequest createAppointmentsRequest) {
    return Flux.create((emitter) -> {
      for (var currentDateTime = createAppointmentsRequest.getStartAt();
           currentDateTime.isBefore(createAppointmentsRequest.getEndAt()); currentDateTime =
               currentDateTime.plusMinutes(createAppointmentsRequest.getHowOftenInMinutes())) {
        for (var i = 1; i <= createAppointmentsRequest.getHowManyParallel(); i++) {
          var newAppointmentEvent = NewAppointmentEvent.builder().startAt(currentDateTime)
              .vaccineCenterId(createAppointmentsRequest.getVaccineCenterId()).build();
          emitter.next(newAppointmentEvent);
        }
      }
      emitter.complete();
    });
  }

  /**
   * Method to handle new appointment event.
   *
   * @param newAppointmentEvent new appointment event payload
   */
  @KafkaListener(topics = NEW_APPOINTMENTS_EVENT_TOPIC)
  public void receive(NewAppointmentEvent newAppointmentEvent) {
    Mono.just(newAppointmentEvent).map(appointmentDbCreator::create)
        .flatMap(appointmentDbRepository::save)
        .doOnNext((appointmentDb) -> log.info(
            "New available appointment saved in DB: " + appointmentDb.toString()))
        .then()
        .subscribe();
  }
}
