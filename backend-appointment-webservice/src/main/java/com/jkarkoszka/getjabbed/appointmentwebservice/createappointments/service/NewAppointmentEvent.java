package com.jkarkoszka.getjabbed.appointmentwebservice.createappointments.service;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Value;

/**
 * New appointment event payload.
 */
@Value
@Builder
public class NewAppointmentEvent {

  LocalDateTime startAt;
  String vaccineCenterId;

}
