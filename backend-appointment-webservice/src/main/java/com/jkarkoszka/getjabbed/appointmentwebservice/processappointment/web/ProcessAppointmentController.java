package com.jkarkoszka.getjabbed.appointmentwebservice.processappointment.web;

import com.jkarkoszka.getjabbed.appointment.web.AppointmentRest;
import com.jkarkoszka.getjabbed.appointment.web.AppointmentRestMapper;
import com.jkarkoszka.getjabbed.appointmentwebservice.processappointment.service.ProcessAppointmentService;
import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Controller which expose endpoint to process appointment.
 */
@RestController
@AllArgsConstructor
public class ProcessAppointmentController {

  private final ProcessAppointmentService processAppointmentService;
  private final AppointmentRestMapper appointmentRestMapper;

  /**
   * Endpoint to process appointment.
   *
   * @param appointmentId                appointment id
   * @param getJabbedAuthenticationToken get jabbed authentication token
   * @return appointments
   */
  @PatchMapping("/appointments/booked/{appointmentId}")
  public Mono<AppointmentRest> findAvailableAppointments(
      @PathVariable("appointmentId") String appointmentId,
      GetJabbedAuthenticationToken getJabbedAuthenticationToken) {
    return Mono.fromSupplier(() -> ProcessAppointmentRequestCreator.create(appointmentId,
            getJabbedAuthenticationToken))
        .flatMap(processAppointmentService::process)
        .map(appointmentRestMapper::map);
  }
}
