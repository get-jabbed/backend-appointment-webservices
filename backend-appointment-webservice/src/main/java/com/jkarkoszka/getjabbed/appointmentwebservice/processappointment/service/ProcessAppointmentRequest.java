package com.jkarkoszka.getjabbed.appointmentwebservice.processappointment.service;

import lombok.Builder;
import lombok.Value;

/**
 * Process appointment request payload.
 */
@Value
@Builder
public class ProcessAppointmentRequest {

  String appointmentId;
  String vaccineCenterId;
}
