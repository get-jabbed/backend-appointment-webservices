package com.jkarkoszka.getjabbed.appointmentwebservice.processappointment.web;

import com.jkarkoszka.getjabbed.appointmentwebservice.processappointment.service.ProcessAppointmentRequest;
import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;

class ProcessAppointmentRequestCreator {

  static ProcessAppointmentRequest create(
      String appointmentId,
      GetJabbedAuthenticationToken getJabbedAuthenticationToken) {
    return ProcessAppointmentRequest.builder()
        .appointmentId(appointmentId)
        .vaccineCenterId(getJabbedAuthenticationToken.getVaccineCenterId())
        .build();
  }
}
