package com.jkarkoszka.getjabbed.appointmentwebservice.processappointment.service;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDb;
import com.jkarkoszka.getjabbed.appointment.db.Status;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.springframework.stereotype.Component;

/**
 * Class to update Appointment to DONE status.
 */
@Component
public class DoneAppointmentUpdater {

  AppointmentDb update(AppointmentDb appointmentDb) {
    return appointmentDb.toBuilder()
        .status(Status.DONE)
        .updatedAt(LocalDateTime.now(ZoneOffset.UTC))
        .build();
  }
}
