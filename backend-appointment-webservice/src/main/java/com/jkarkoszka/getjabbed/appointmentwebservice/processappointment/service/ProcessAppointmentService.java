package com.jkarkoszka.getjabbed.appointmentwebservice.processappointment.service;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDbRepository;
import com.jkarkoszka.getjabbed.appointment.service.Appointment;
import com.jkarkoszka.getjabbed.appointment.service.AppointmentMapper;
import lombok.AllArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * Service to process appointment.
 */
@Component
@AllArgsConstructor
public class ProcessAppointmentService {

  private final AppointmentDbRepository appointmentDbRepository;
  private final DoneAppointmentUpdater doneAppointmentUpdater;
  private final AppointmentMapper appointmentMapper;

  /**
   * Method to process appointment.
   *
   * @param processAppointmentRequest process appointment request payload
   * @return appointment
   */
  public Mono<Appointment> process(ProcessAppointmentRequest processAppointmentRequest) {
    var vaccineCenterId = processAppointmentRequest.getVaccineCenterId();
    return Mono.fromSupplier(processAppointmentRequest::getAppointmentId)
        .map(ObjectId::new)
        .flatMap(appointmentId -> appointmentDbRepository.findBookedAppointment(
                appointmentId,
                vaccineCenterId)
            .switchIfEmpty(Mono.error(new BookedAppointmentNotFoundException(appointmentId,
                vaccineCenterId))))
        .map(doneAppointmentUpdater::update)
        .flatMap(appointmentDbRepository::save)
        .map(appointmentMapper::map);
  }
}
