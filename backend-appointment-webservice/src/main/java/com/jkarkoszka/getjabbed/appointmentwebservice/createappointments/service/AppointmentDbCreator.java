package com.jkarkoszka.getjabbed.appointmentwebservice.createappointments.service;

import com.jkarkoszka.getjabbed.appointment.db.AppointmentDb;
import com.jkarkoszka.getjabbed.appointment.db.Status;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

/**
 * Class to create appointment DB.
 */
@Component
public class AppointmentDbCreator {

  AppointmentDb create(NewAppointmentEvent newAppointmentEvent) {
    return AppointmentDb.builder()
        .id(new ObjectId())
        .at(newAppointmentEvent.getStartAt())
        .vaccineCenterId(newAppointmentEvent.getVaccineCenterId())
        .status(Status.AVAILABLE)
        .createdAt(LocalDateTime.now(ZoneOffset.UTC))
        .updatedAt(LocalDateTime.now(ZoneOffset.UTC))
        .build();
  }
}
