package com.jkarkoszka.getjabbed.appointmentwebservice;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration of the application.
 */
@Configuration
@ConfigurationProperties(prefix = "getjabbed.appointment-webservices")
@Data
public class AppProperties {

  private Kafka kafka;

  @Data
  static class Kafka {

    private String bootstrapServers;
    private String clientId;
    private String groupId;
    private String autoOffsetRestConfig;
  }
}
