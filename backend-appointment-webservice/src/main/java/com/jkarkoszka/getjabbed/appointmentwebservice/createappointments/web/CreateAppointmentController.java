package com.jkarkoszka.getjabbed.appointmentwebservice.createappointments.web;

import com.jkarkoszka.getjabbed.appointmentwebservice.createappointments.service.CreateAppointmentService;
import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Controller to create appointment.
 */
@RestController
@AllArgsConstructor
public class CreateAppointmentController {

  private final CreateAppointmentsRequestMapper createAppointmentsRequestMapper;
  private final CreateAppointmentService createAppointmentService;

  /**
   * Method to create appointment.
   *
   * @param createAppointmentsRestRequest create appointments rest request payload
   * @param getJabbedAuthenticationToken  get jabbed authentication token
   * @return empty
   */
  @PostMapping("/appointments")
  public Mono<Void> createAppointments(
      @RequestBody CreateAppointmentsRestRequest createAppointmentsRestRequest,
      GetJabbedAuthenticationToken getJabbedAuthenticationToken) {
    return Mono.fromSupplier(() -> createAppointmentsRequestMapper
            .map(createAppointmentsRestRequest, getJabbedAuthenticationToken))
        .flatMap(createAppointmentService::createNewAppointmentEventsAndSendToQueue);
  }
}
