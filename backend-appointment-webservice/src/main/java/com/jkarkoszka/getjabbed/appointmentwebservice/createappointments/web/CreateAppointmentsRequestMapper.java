package com.jkarkoszka.getjabbed.appointmentwebservice.createappointments.web;

import com.jkarkoszka.getjabbed.appointmentwebservice.createappointments.service.CreateAppointmentsRequest;
import com.jkarkoszka.getjabbed.security.filter.GetJabbedAuthenticationToken;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

/**
 * Mapper CreateAppointmentsRequest&GetJabbedAuthenticationToken/CreateAppointmentsRestRequest.
 */
@Component
class CreateAppointmentsRequestMapper {

  CreateAppointmentsRequest map(CreateAppointmentsRestRequest restRequest,
                                       GetJabbedAuthenticationToken getJabbedAuthenticationToken) {
    return CreateAppointmentsRequest.builder()
        .id(new ObjectId().toHexString())
        .startAt(restRequest.getStartAt())
        .endAt(restRequest.getEndAt())
        .howManyParallel(restRequest.getHowManyParallel())
        .howOftenInMinutes(restRequest.getHowOftenInMinutes())
        .vaccineCenterId(getJabbedAuthenticationToken.getVaccineCenterId())
        .build();
  }
}
