package com.jkarkoszka.getjabbed.appointmentwebservice.createappointments.web;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Value;

/**
 * Create appointments rest request payload.
 */
@Value
@Builder
public class CreateAppointmentsRestRequest {

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  LocalDateTime startAt;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
  LocalDateTime endAt;
  int howOftenInMinutes;
  int howManyParallel;

}
