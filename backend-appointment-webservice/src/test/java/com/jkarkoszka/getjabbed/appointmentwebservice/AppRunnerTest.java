package com.jkarkoszka.getjabbed.appointmentwebservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class AppRunnerTest {

  @Test
  void shouldStartCorrectly() {
  }

}